import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src')

from binarysearchtree import Node, BinarySearchTree

class NewNode(Node):
    def __init__(self, key):
        super().__init__(key)
        self.left = None
        self.right = None

def test_insert():
    bst = BinarySearchTree()

    """Test Basic Tree Node creation, insert
    """
    root = None
    root = bst.insert(root, 50)    
    root = bst.insert(root, 30)
    root = bst.insert(root, 20)
    root = bst.insert(root, 40)
    root = bst.insert(root, 70)
    root = bst.insert(root, 60)
    root = bst.insert(root, 80)

    """ Get the tree as a list
    """
    l = bst.inorder(root)
    assert len(l) == 7
    assert l == [20, 30, 40, 50, 60, 70, 80]

    """ Get min/max tree nodes from BST
    """
    assert bst.maximum(root).value == 80
    assert bst.minimum(root).value == 20

    """ Test iterative and recursive search the given key"""
    assert bst.i_search(root, 80) == True
    assert bst.i_search(root, 0) == False

    assert bst.r_search(root, 80) == True
    assert bst.r_search(root, 0) == False

    """ delete node test and check the tree nodes after"""
    root = bst.deleteNode(root, 20)
    bst.clearList()
    assert bst.inorder(root) == [30, 40, 50, 60, 70, 80]

    root = bst.deleteNode(root, 80)
    bst.clearList()
    assert bst.inorder(root) == [30, 40, 50, 60, 70]

    root = bst.deleteNode(root, 50)
    bst.clearList()
    assert bst.inorder(root) == [30, 40, 60, 70]

def test_presuc():
    bst = BinarySearchTree()

    """Test Basic Tree Node creation, insert
    """
    root = None
    root = bst.insert(root, 50)
    root = bst.insert(root, 30)
    root = bst.insert(root, 20)
    root = bst.insert(root, 40)
    root = bst.insert(root, 70)
    root = bst.insert(root, 60)
    root = bst.insert(root, 80)

    """ Get the tree as a list
    """
    l = bst.inorder(root)
    assert len(l) == 7
    assert l == [20, 30, 40, 50, 60, 70, 80]

    # Driver program to test above function
    key = 60 #Key to be searched in BST

    bst.findPreSuc(root, key)
    #if pre is not None:
    #    assert pre.value == 60
    #if suc is not None:
    #    assert suc.value == 70
    #bst.findPreSuc(root, key)

