class Node:
    def __init__(self, key):
        self.left = None
        self.right = None
        self.value = key

class BinarySearchTree:

    def __init__(self):
        self.l = []

    """ Basic insert node
    input: root node, key
    output: updated tree root node
    """
    def insert(self, root, key):
        if root is None:
            return Node(key)

        if root.value == key:
            return root

        if root.value < key:
            root.right = self.insert(root.right, key)
        else:
            root.left = self.insert(root.left, key)

        return root

    """clears the list helper object
    """
    def clearList(self):
        self.l = []

    """ inorder and return node values as a list
    input: root node
    output: list with node values
    """
    def inorder(self, root):
        if root:
            self.inorder(root.left)
            self.l.append(root.value)
            self.inorder(root.right)
        return self.l

    """get max value node in tree
    input: root node
    output: node which has maximum value
    """
    @classmethod
    def maximum(cls, root):
        while root.right is not None:
            root = root.right
        return root

    """ minumum value node in Tree
    input: root node
    output: node which has minimum value
    """
    @classmethod
    def minimum(cls, root):
        while root.left is not None:
            root = root.left
        return root

    """ Iterative BST serch
    input: root node and key
    output: None if not founf key
            node which has input key
    """
    @classmethod
    def i_search(cls, root, key):
        while root is not None and key != root.value:
            if key < root.value:
                root = root.left
            else:
                root = root.right
        if root is None:
            return False

        if root.value == key:
            return True

        return False

    """ Recursive BST serch
    input: root node and key
    output: None if not founf key
            node which has input key
    """
    def r_search(self, root, key):
        if root is None:
            return False

        if root.value == key:
            return True

        if key < root.value:
            return self.r_search(root.left, key)

        if key > root.value:
            return self.r_search(root.right, key)

        return False

    # Given a binary search tree and a key, this function
    # delete the key and returns the new root

    def deleteNode(self, root, key):

        # Base Case
        if root is None:
            return root

        # If the key to be deleted
        # is smaller than the root's
        # key then it lies in  left subtree
        if key < root.value:
            root.left = self.deleteNode(root.left, key)

        # If the kye to be delete
        # is greater than the root's key
        # then it lies in right subtree
        elif key > root.value:
            root.right = self.deleteNode(root.right, key)

        # If key is same as root's key, then this is the node
        # to be deleted
        else:

            # Node with only one child or no child
            if root.left is None:
                temp = root.right
                root = None
                return temp

            if root.right is None:
                temp = root.left
                root = None
                return temp

            # Node with two children:
            # Get the inorder successor
            # (smallest in the right subtree)
            temp = self.minimum(root.right)

            # Copy the inorder successor's
            # content to this node
            root.value = temp.value

            # Delete the inorder successor
            root.right = self.deleteNode(root.right, temp.value)

        return root

    """Inorder predecessor and successor for a given key in BST
    """
    def findPreSuc(self, root, key):
        pre = None
        suc = None

        if root.left is not None or root.right is not None:
            print(f"root-val:{root.value}, root-left:{root.left.value}, root-right:{root.right.value}, key:{key}")

        # Base Case
        if root is None:
            print("None:None")
            return None, None

        # If key is present at root
        if root.value == key:

            # the maximum value in left subtree is predecessor
            if root.left is not None:
                temp = root.left
                while temp.right:
                    temp = temp.right
                pre = temp


            # the minimum value in right subtree is successor
            if root.right is not None:
                temp = root.right
                while temp.left:
                    temp = temp.left
                suc = temp
            print(f"pre:{pre}, suc:{suc}")
            return pre, suc

        # If key is smaller than root's key, go to left subtree
        if root.value > key:
            suc = root
            self.findPreSuc(root.left, key)

        else: # go to right subtree
            pre = root
            self.findPreSuc(root.right, key)

